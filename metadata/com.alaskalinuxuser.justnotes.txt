Categories:Writing
License:Apache-2.0
Web Site:https://thealaskalinuxuser.wordpress.com/
Source Code:https://github.com/alaskalinuxuser/app_justnotes
Issue Tracker:https://github.com/alaskalinuxuser/app_justnotes/issues

Auto Name:Just Notes
Summary:Take notes
Description:
This is a no frills, simple, easy to use note taking application for those times
that you just want to get straight to the point. Make and take notes, as well as
save and delete old notes. A confirmation box will open before you delete a note
to make sure that you really want to get rid of it.

If you are just starting out on app design, feel free to check out my "just"
apps and "small" apps for easy to understand and usable code.
.

Repo Type:git
Repo:https://github.com/alaskalinuxuser/app_justnotes

Build:1.0,1
    commit=26003a90a5c0e73f4adbd4fa89867f402cc226f6
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:2
