Categories:Internet
License:GPL-3.0+
Web Site:https://getdelta.org/
Source Code:https://github.com/r10s/messenger-android
Issue Tracker:https://github.com/r10s/messenger-android/issues
Changelog:https://github.com/r10s/messenger-android/blob/HEAD/CHANGELOG.md
Donate:https://getdelta.org/en/support

Auto Name:Delta Chat
Summary:Communicate instantly via e-mail
Description:
Delta Chat is a project that aims to create a messaging app that is completely
compatible to the existing e-mail infrastructure.

So, with Delta Chat you get the ease of well-known messengers with the reach of
e-mail. Moreover, you're independent from other companies or services -- as your
data are not related to Delta Chat, you won't even add new dependecies here.

Some features at a glance:

* Fast by the use of Push-IMAP
* Largest userbase -- receivers not using Delta Chat can be reached as well
* Compatible -- not only to itself
* Elegant and simple user interface
* Distributed system
* No Spam -- only messages of known users are shown by default
* Reliable -- safe for professional use
* Trustworthy -- can even be used for business messages
* Fully OpenSource and Standards based
.

Repo Type:git
Repo:https://github.com/r10s/messenger-android

Build:0.1.15,143
    commit=v0.1.15
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.16,153
    commit=v0.1.16
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.17,163
    commit=v0.1.17
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.18,173
    commit=v0.1.18
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.20,193
    commit=v0.1.20
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.21,203
    commit=v0.1.21
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.22,213
    commit=v0.1.22
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Build:0.1.23,223
    commit=v0.1.23
    subdir=MessengerProj
    submodules=yes
    gradle=fat
    build=$$NDK$$/ndk-build && \
        gradle nativeLibsToJar

Auto Update Mode:Version v%v
Update Check Mode:Tags
Vercode Operation:%c*10 + 3
Current Version:0.1.23
Current Version Code:223
